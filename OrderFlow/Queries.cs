﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iosDB
{
    public class Queries
    {
        public string qHowManyTransmit(int tMinutes,string dbname,string cutoff, string RptHours)
        {
            string TheMinutes = tMinutes.ToString();
            string qStr = "";

            qStr += CountPrefix(dbname);

            //Sent long ago
            qStr += "datediff(mi,mo.TransDate,GETDATE()) > " + TheMinutes + " ";

            //not transmitted
            qStr += "and ";
            qStr += "mo.TransmitDate is NULL ";

            //not needing approval; Orders needing approval picked up in TooLongToApprove
            qStr += "and mo.RepApprovalRequired != 1 ";

            //If MHW, not past cutoffs [MHW so weird]
            if (dbname == "mhw_prod")
                qStr += "and mo.OrderNumber not in (" + MHWOrdersAfterCutoff(dbname) + ") ";

            qStr += LegitPostfix(cutoff, RptHours);

            return qStr;
        }

        public string qWhichOrdersTransmit(int tMinutes, string dbname, string cutoff, string RptHours)
        {
            string TheMinutes = tMinutes.ToString();
            string qStr = "";

            qStr += WhichPrefix(dbname);
            
            //Sent long ago
            qStr += "datediff(mi,mo.TransDate,GETDATE()) > " + TheMinutes + " ";

            //not transmitted
            qStr += "and ";
            qStr += "mo.TransmitDate is NULL ";

            //not needing approval; Orders needing approval picked up in TooLongToApprove
            qStr += "and mo.RepApprovalRequired != 1 ";

            //If MHW, not past cutoffs [MHW so weird]
            if (dbname == "mhw_prod")
                qStr += "and mo.OrderNumber not in (" + MHWOrdersAfterCutoff(dbname) + ") ";

            qStr += LegitPostfix(cutoff, RptHours);

            //order by
            qStr += "order by mo.OrderNumber";

            return qStr;
        }

        public string qHowManyConfirm(int tMinutes, string dbname, string cutoff, string RptHours)
        {
            string TheMinutes = tMinutes.ToString();
            string qStr = "";

            qStr += CountPrefix(dbname);

            //Transmitted to distributor long ago
            qStr += "datediff(mi,mo.TransmitDate,GETDATE()) > " + TheMinutes + " ";

            //not confirmed
            qStr += "and ";
            qStr += "mo.FullFilledDate is NULL ";

            qStr += LegitPostfix(cutoff, RptHours);

            return qStr;
        }

        public string qWhichOrdersConfirm(int tMinutes, string dbname, string cutoff, string RptHours)
        {
            string TheMinutes = tMinutes.ToString();
            string qStr = "";

            qStr += WhichPrefix(dbname);

            //Transmitted to distributor long ago
            qStr += "datediff(mi,mo.TransmitDate,GETDATE()) > " + TheMinutes + " ";

            //not confirmed
            qStr += "and ";
            qStr += "mo.FullFilledDate is NULL ";

            qStr += LegitPostfix(cutoff, RptHours);

            //order by
            qStr += "order by mo.OrderNumber";

            return qStr;
        }
        public string qHowManyDelivery(string dbname, string cutoff, string RptHours)
        {
            string qStr = "";

            qStr += CountPrefix(dbname);

            //Too late to deliver
            qStr += "datediff(mi,convert(date,GETDATE()),convert(date,mo.ActivateDate)) <  0 "; //ActivateDate only good to the date level

            //not confirmed
            qStr += "and ";
            qStr += "mo.FullFilledDate is NULL ";

            qStr += LegitPostfix(cutoff, RptHours);

            return qStr;
        }

        public string qWhichOrdersDelivery(string dbname, string cutoff, string RptHours)
        {
            string qStr = "";

            qStr += WhichPrefix(dbname);

            //Too late to deliver
            qStr += "datediff(mi,convert(date,GETDATE()),convert(date,mo.ActivateDate)) <  0 "; //ActivateDate only good to the date level

            //not confirmed
            qStr += "and ";
            qStr += "mo.FullFilledDate is NULL ";

            qStr += LegitPostfix(cutoff, RptHours);

            //order by
            qStr += "order by mo.OrderNumber";

            return qStr;
        }

        public string qHowManyApproval(int tMinutes, string dbname, string cutoff, string RptHours)
        {
            string TheMinutes = tMinutes.ToString();
            string qStr = "";

            qStr += CountPrefix(dbname);

            //Needs approval and hasn't been
            qStr += "(RepApprovalRequired=1) and (RepApprovalDate is NULL) ";

            //Sent long ago
            qStr += "and (datediff(mi,mo.TransDate,GETDATE()) > " + TheMinutes + ") ";

            qStr += LegitPostfix(cutoff, RptHours);

            return qStr;
        }

        public string qWhichOrdersApproval(int tMinutes, string dbname, string cutoff, string RptHours)
        {
            string TheMinutes = tMinutes.ToString();
            string qStr = "";

            qStr += WhichPrefix(dbname);

            //Needs approval and hasn't been
            qStr += "(RepApprovalRequired=1) and (RepApprovalDate is NULL) ";

            //Sent long ago
            qStr += "and (datediff(mi,mo.TransDate,GETDATE()) > " + TheMinutes + ") ";

            qStr += LegitPostfix(cutoff, RptHours);

            //order by
            qStr += "order by mo.OrderNumber";

            return qStr;
        }

        private string CountPrefix(string dbname)
        {
            string preStr = "";

            preStr += "select count(OrderNumber) ";
            preStr += "from " + MOTable(dbname) + " ";
            preStr += "where ";

            return preStr;
        }

        private string WhichPrefix(string dbname)
        {
            string preStr = "";

            preStr += "select mo.OrderNumber,a1.CompanyName as Acct,a2.ContactName as Rep ";
            preStr += "from " + MOTable(dbname) + " ";
            preStr += "inner join " + AcctTable(dbname) + " a1 ON (mo.AccountID = a1.AccountID) ";
            preStr += "inner join " + AcctTable(dbname) + " a2 ON (mo.DSRAccount = a2.AccountID) ";
            preStr += "where ";

            return preStr;
        }

        private string LegitPostfix(string cutoff, string ReportHours)
        {
            string postStr = "";

            //not cancelled
            postStr += "and ";
            postStr += "mo.CancellationDate is NULL ";

            //Not after cutoff
            //postStr += "and ";
            //postStr += "mo.TransDate < '" + CutOffTime(cutoff) + "' ";

            //In the past forty-eight hours
            postStr += "and ";
            postStr += "datediff(hh,mo.TransDate,GETDATE()) < " + ReportHours + " ";

            return postStr;
        }

        private string MHWOrdersAfterCutoff(string dbname)
        {
            string mhwStr = "";

            mhwStr += "select mo1.Ordernumber ";
            mhwStr += "from " + MOTable(dbname,"1") + " ";
            mhwStr += "inner join " + AcctTable(dbname) + " a ON (mo1.AccountID = a.AccountID) ";

            mhwStr += "where ";
            mhwStr += "((upper(a.State) != 'CA') and (CAST(transdate as date) = CAST(getdate() as date)) and (CAST(transdate as time) > CAST('16:01:00' as time))) ";
            mhwStr += "or ";
            mhwStr += "((upper(a.State) = 'CA') and (CAST(transdate as date) = CAST(getdate() as date)) and (CAST(transdate as time) > CAST('17:01:00' as time)))";

            return mhwStr;
        }
        
        //================================================================================================

        public string MOTable(string dbname,string sub = "")
        {
            string tablestr = "";

            tablestr = dbname + ".dbo.MasterOrder mo" + sub;

            return tablestr;
        }

        public string AcctTable(string dbname)
        {
            string tablestr = "";

            tablestr = dbname + ".dbo.Accounts";

            return tablestr;
        }

        public string CutOffTime(string cutoff)
        {
            string timestr; //ISO-8601
            string yearpart, monthpart, daypart;
            string timepart = "T" + cutoff;

            yearpart = DateTime.Now.ToString("yyyy");
            monthpart = DateTime.Now.ToString("MM");
            daypart = DateTime.Now.ToString("dd");

            timestr = yearpart + "-" + monthpart + "-" + daypart + timepart;

            return timestr;
        }

    }//class
}//namespace
