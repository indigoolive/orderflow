﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LOGGING;

namespace OrderFlow
{
    public static class Globals
    {
        public static Logger log;
        
        public static Logger InitLogger(string logpath)
        {
            return new Logger(logpath);
        }
    
    }//class
}//namespace
