﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Net;

using LOGGING;
using INI;
using iosDB;
using EMAIL;

namespace OrderFlow
{
    class CheckOrderFlow
    {
        Queries q = new Queries();

        Logger log;
        bool debug2log; //Dump debug info to log file

        string MailBody = "";
        string newline = "\n";

        ClientDatabase db_main = null;

        string IPDataSource;
        string HostNameDataSource;
        IPHostEntry ip_host_entry;

        string databasename;
        string cutofftime;
        string ReportHourWindow; //For how many hours will we report a "bad" order??
        
        public void MainCheck(string WhichDistributor) //arg[0] from command line
        {
            //Define/set variables    
            INIFile ini = new INIFile();
            EMail mail = new EMail();

            bool SendAnEmail = false;

            //Read .ini files for additional variables
            string logpath;

            bool CheckingTransmit;
            int TransmitMinutes;

            bool CheckingConfirm;
            int ConfirmMinutes;

            bool CheckingDelivery;

            bool CheckingApproval;
            int ApprovalMinutes;

            string SMTPHost;
            string SMTPUser;
            string SMTPPW;
            int SMTPPort;

            string MailTo;
            string MailFrom;
            string MailSubject;

            debug2log = Convert.ToBoolean(ini.INIReadInt(WhichDistributor, "Debug"));

            debugline("Starting .ini file read for " + WhichDistributor);

            databasename = ini.INIReadString(WhichDistributor, "DatabaseName");
            cutofftime = ini.INIReadString(WhichDistributor, "CutoffTime");
            logpath = ini.INIReadString(WhichDistributor, "LogPath");
            ReportHourWindow = ini.INIReadString(WhichDistributor, "ReportHourWindow");

            debugline("Read main ini stuff " + WhichDistributor);

            CheckingTransmit = Convert.ToBoolean(ini.INIReadInt(WhichDistributor, "CheckingTransmit"));
            TransmitMinutes = ini.INIReadInt(WhichDistributor, "TransmitMinutes");

            debugline("Read transmit ini stuff " + WhichDistributor);

            CheckingConfirm = Convert.ToBoolean(ini.INIReadInt(WhichDistributor, "CheckingConfirm"));
            ConfirmMinutes = ini.INIReadInt(WhichDistributor, "ConfirmMinutes");

            debugline("Read confirm ini stuff " + WhichDistributor);

            CheckingDelivery = Convert.ToBoolean(ini.INIReadInt(WhichDistributor, "CheckingDelivery"));

            debugline("Read delivery ini stuff " + WhichDistributor);

            CheckingApproval = Convert.ToBoolean(ini.INIReadInt(WhichDistributor, "CheckingApproval"));
            ApprovalMinutes = ini.INIReadInt(WhichDistributor, "ApprovalMinutes");

            debugline("Read approval ini stuff " + WhichDistributor);

            MailTo = ini.INIReadString(WhichDistributor, "MailTo");
            MailFrom = ini.INIReadString(WhichDistributor, "MailFrom");
            MailSubject = ini.INIReadString(WhichDistributor, "MailSubject");

            debugline("Read mail ini stuff " + WhichDistributor);

            SMTPHost = ini.INIReadString("SMTP", "SMTPHost");
            SMTPUser = ini.INIReadString("SMTP", "SMTPUser");
            SMTPPW = ini.INIReadString("SMTP", "SMTPPW");
            SMTPPort = ini.INIReadInt("SMTP", "SMTPPort");

            debugline("Read SMTP ini stuff " + WhichDistributor);

            HostNameDataSource = ini.INIReadString("DataSource", "HostName");

            debugline("Read DataSource ini stuff " + WhichDistributor);

            //Code starts

            debugline("Starting " + WhichDistributor);

            //Init log

            log = Globals.InitLogger(logpath);
            log.WriteInfo("Starting " + WhichDistributor);

            //Get IP Address from Host Name
            ip_host_entry = Dns.GetHostEntry(HostNameDataSource);
            foreach(IPAddress address in ip_host_entry.AddressList) //should just be one
            {
                IPDataSource = address.ToString();
            }

            //Open db
            try
            {
                db_main = new ClientDatabase();
                db_main.MakeConnection(IPDataSource);
            }
            catch (Exception e_conn)
            {
                log.WriteError("Unable to connect to database");
                log.WriteError("Message: " + e_conn.Message);
                return;
            }

            //If the difference between when a rep sends an order [MasterOrder.Transdate]
            //and when we make an order ready for pickup [MasterOrder.Transmitdate]
            //is "too many minutes"
            //Send a list of the orders and an email message

            if (CheckingTransmit)
                if (TooLongToTransmit(TransmitMinutes))
                {
                    SendAnEmail = true;
                    BuildTransmitText(TransmitMinutes);
                }

            //If the difference between when an order is ready for pickup [MasterOrder.TransmitDate]
            //and when we get a confirmation [MasterOrder.FullfilledDate]
            //is "too many minutes"
            //Send a list of the orders and an email message

            if (CheckingConfirm)
                if (TooLongToConfirm(ConfirmMinutes))
                {
                    SendAnEmail = true;
                    BuildConfirmText(ConfirmMinutes);
                }

            //If the Delivery Date [MasterOrder.ActivateDate]
            //has past
            //Send a list of the orders and an email message

            if (CheckingDelivery)
                if (PastDeliveryDate())
                {
                    SendAnEmail = true;
                    BuildDeliveryText();
                }

            //If an order needs approval [MasterOrder.RepApprovalRequired]
            //and hasn't been approved [MasterOrder.RepApprovalDate is null]
            //for "too many minutes"
            //Send a list of the orders and an email message

            if (CheckingApproval)
                if (TooLongToApprove(ApprovalMinutes))
                {
                    SendAnEmail = true;
                    BuildApprovalText(ApprovalMinutes);
                }

            //If we need to email now is the time.

            if (SendAnEmail)
            {
                mail.to = MailTo;
                mail.from = MailFrom;
                mail.subject = MailSubject;

                debugline("MailBody is: " + MailBody);

                mail.body = MailBody;

                mail.send(SMTPHost, SMTPUser, SMTPPW, SMTPPort);

                log.WriteInfo("Sent an email to " + WhichDistributor);
            }
            else
            {
                log.WriteInfo("No need to send an email to " + WhichDistributor);
            }

            log.WriteInfo("Ending " + WhichDistributor);

        }//MainCheck

        //=====================================================================================================

        private bool TooLongToTransmit(int tMinutes)
        {
            return TookTooLong(q.qHowManyTransmit(tMinutes, databasename, cutofftime, ReportHourWindow));
        }

        private void BuildTransmitText(int TransmitMinutes)
        {
            MailBody += newline; //start down a line
            MailBody += "Indigo Olive has received orders from reps more than " + TransmitMinutes.ToString() + " minutes ago." + newline;
            MailBody += "Please be sure you are picking up order files in a timely manner. A new order file is not created until an old order file is picked up." + newline;
            MailBody += "If you see no order files to be picked in the FTP subdirectory, please contact Indigo Olive technical support. The problem may be on our end." + newline;

            MailBody += "Affected Orders:" + newline;
            AddAffectedOrders(q.qWhichOrdersTransmit(TransmitMinutes, databasename, cutofftime, ReportHourWindow));

        }//BuildTransmitText

        private bool TooLongToConfirm(int tMinutes)
        {
            return TookTooLong(q.qHowManyConfirm(tMinutes, databasename, cutofftime, ReportHourWindow));
        }

        private void BuildConfirmText(int ConfirmMinutes)
        {
            MailBody += newline; //start down a line
            MailBody += "Indigo Olive sent you orders that have not been confirmed for more than " + ConfirmMinutes + " minutes." + newline;
            MailBody += "Now would be a good time to inspect your ERP system and make sure that everything is working properly." + newline;

            MailBody += "Affected Orders:" + newline;
            AddAffectedOrders(q.qWhichOrdersConfirm(ConfirmMinutes, databasename, cutofftime, ReportHourWindow));

        }//BuildConfirmText

        private bool PastDeliveryDate()
        {
            return TookTooLong(q.qHowManyDelivery(databasename, cutofftime, ReportHourWindow));
        }

        private void BuildDeliveryText()
        {
            MailBody += newline; //start down a line
            MailBody += "Some orders have been entered into your system that cannot be delivered by the date chosen by the rep." + newline;

            MailBody += "Affected Orders:" + newline;
            AddAffectedOrders(q.qWhichOrdersDelivery(databasename, cutofftime, ReportHourWindow));

        }//BuildDeliveryText

        private bool TooLongToApprove(int tMinutes)
        {
            return TookTooLong(q.qHowManyApproval(tMinutes, databasename, cutofftime, ReportHourWindow));
        }
        
        private void BuildApprovalText(int ApprovalMinutes)
        {
            MailBody += newline;
            MailBody += "You have orders that have been waiting for approval for more than " + ApprovalMinutes.ToString() + " minutes." + newline;
            MailBody += "Please login to the admin website at www2.beverageone.com and approve them at your convenience." + newline;
            MailBody += "Left hand menu; ORDERS; Approve orders." + newline;

            MailBody += "Affected Orders:" + newline;
            AddAffectedOrders(q.qWhichOrdersApproval(ApprovalMinutes, databasename, cutofftime, ReportHourWindow));

        }//BuildApprovalText

        private bool TookTooLong(string WhichQueryString)
        {
            bool brval = false;

            var TTL = (Object)null;
            int iTTL;

            debugline("TookTooLong query is " + WhichQueryString);

            try
            {
                TTL = db_main.ExecuteScalar(WhichQueryString);
            }
            catch (Exception e)
            {
                log.WriteError("Error in TookTooLong");
                log.WriteError("Query: " + WhichQueryString);
                log.WriteError("Message: " + e.Message);
                return brval;
            }

            iTTL = Convert.ToInt32(TTL);
            log.WriteInfo("TookTooLong count = " + iTTL.ToString());

            brval = (iTTL > 0);

            return brval;
        }

        private void AddAffectedOrders(string WhichQueryString)
        {
            string OrderNumber, Acct, Rep;
            DataTable table = new DataTable();

            try
            {
                table = db_main.GetGenericTable(WhichQueryString);
            }
            catch (Exception e)
            {
                log.WriteError("Unable to open AddAffectedOrders query");
                log.WriteError("Query: " + WhichQueryString);
                log.WriteError("Message: " + e.Message);
                table = null;
            }

            if (table != null)
                foreach (DataRow roe in table.Rows)
                {
                    OrderNumber = roe.Field<int>("OrderNumber").ToString();
                    Acct = roe.Field<string>("Acct");
                    Rep = roe.Field<string>("Rep");

                    MailBody += OrderNumber + "   " + Acct + "   " + Rep + newline;
                }
            MailBody += newline; //blank

        }//AddAffectedOrders

        private void debugline(string debugstr)
        {
            if (debug2log)
                Console.WriteLine(debugstr);
        }

    }//class
}//namespace
