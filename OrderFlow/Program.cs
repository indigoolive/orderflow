﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LOGGING;

namespace OrderFlow
{
    class Program
    {
        static void Main(string[] args)

        {
            if (args.Length == 0)
            {
                Console.WriteLine("Error: Use the form OrderFlow INISectionName");
            }
            else
            {
                CheckOrderFlow cof = new CheckOrderFlow();
                cof.MainCheck(args[0]);
            }

        }//Main
    }//class
}//namespace
