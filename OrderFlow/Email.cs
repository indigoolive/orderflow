﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Mail;

using LOGGING;
using INI;

namespace EMAIL
{
    public class EMail
    {
        private MailMessage m;

        public EMail()
        {
            m = new MailMessage();
            m.IsBodyHtml = false;
        }//constructor

        public bool isHTML
        {
            set
            {
                m.IsBodyHtml = value;
            }
        }

        public string to
        {
            set
            {
                m.To.Add(value);
            }
        }

        public string from
        {
            set
            {
                m.Sender = m.From = new MailAddress(value);
            }
        }
        public string subject
        {
            set
            {
                m.Subject = value;
            }
        }

        public string body
        {
            get
            {
                return body;
            }
            set
            {
                m.Body = value;
            }
        }

        public void send(string Host = "", string UserName = "", string Password = "",int Port=25)
        {
            SmtpClient sc = new SmtpClient();

            sc.DeliveryMethod = SmtpDeliveryMethod.Network;
            sc.UseDefaultCredentials = false;
            sc.Port = Port;
            //sc.EnableSsl = true; //WWP; Added 22JUL2014 to MHW for smtp.gmail.com

            if (Host == "") //ini file hosed probably on purpose
            {
                return;
            }

            sc.Host = Host;

            sc.Credentials = new System.Net.NetworkCredential(UserName, Password);

            try
            {
                sc.Send(m);
            }
            catch (SmtpException e)
            {
                throw e;
            }
        } //send

    } //class
}//namespace
