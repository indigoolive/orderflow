﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace LOGGING
{
    public enum LogType
    {
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public class Logger
    {
        // Privates
        private bool isReady = false;
        private StreamWriter swLog;
        private string strLogFile;

        // Constructors
        public Logger(string LogFileName = "") 
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;
            string EXEName = AppDomain.CurrentDomain.FriendlyName;
            EXEName = path + EXEName.Substring(0, EXEName.Length - 4); //trim file ext

            LogFileName = (LogFileName == "") ? EXEName : LogFileName;

            string FN = LogFileName + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log";

            this.strLogFile = FN;
            openFile();
            closeFile();
        }//default constructor

        /*
		public Logger(string LogFileName)
        {
			this.strLogFile = LogFileName;
			openFile();
			_writelog("");
			closeFile();
		}
         */

        private void openFile()
        {
            try
            {
                swLog = File.AppendText(strLogFile);
                isReady = true;
            }
            catch
            {
                isReady = false;
            }
        }

        private void closeFile()
        {
            if (isReady)
            {
                try
                {
                    swLog.Close();
                }
                catch
                {

                }
            }
        }

        public void WriteLine(string message, LogType logtype)
        {
            string stub;

            switch (logtype)
            {
                case LogType.Info:
                    stub = "Ok     :";
                    break;
                case LogType.Warning:
                    stub = "Warning:";
                    break;
                case LogType.Error:
                    stub = "ERROR  :";
                    break;
                default:
                    stub = "???    :";
                    break;
            }

            stub += DateTime.Now.ToString("HH:mm:ss ");
            stub += message;

            openFile();
            _writelog(stub);
            closeFile();

            //Console.WriteLine(stub);
        }

        public void WriteError(string message)
        {
            this.WriteLine(message, LogType.Error);
        }

        public void WriteWarning(string message)
        {
            this.WriteLine(message, LogType.Warning);
        }

        public void WriteInfo(string message)
        {
            this.WriteLine(message, LogType.Info);
        }

        private void _writelog(string msg)
        {
            if (isReady)
            {
                swLog.WriteLine(msg);
            }
            else
            {
                Console.WriteLine("Error -> cannot write to log file.");
            }
        }

    }
} // Logger class 