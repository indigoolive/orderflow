﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

using LOGGING;

namespace iosDB
{
    public class ClientDatabase
    {
        //private string ConnString = "server=(local);database=master;UID=sa;PWD=bev1;";
        //private string Win10ConnString = @"Server=(localdb)\MSSQLLocalDB;Integrated Security=SSPI";

        public Queries q = new Queries();

        private SqlConnectionStringBuilder scsb = new SqlConnectionStringBuilder();

        private SqlConnection connection;

        //default constructor

        public void MakeConnection()
        {
            //MSDE
            //scsb.DataSource = @"(local)";
            ////scsb.NetworkLibrary = "DBMSLPCN";
            ////scsb.InitialCatalog = "master";
            //scsb.IntegratedSecurity = false;
            //scsb.UserID = "sa";
            //scsb.Password = "bev1";
            //scsb.ConnectTimeout = 60;

            //LocalDB
            //scsb.DataSource = @"(localdb)\MSSQLLocalDB";
            ////scsb.NetworkLibrary = "DBMSLPCN";
            ////scsb.InitialCatalog = "master";
            //scsb.IntegratedSecurity = true;
            //scsb.UserID = "sa";
            //scsb.Password = "bev1";
            //scsb.ConnectTimeout = 60;

            //On DB2008
            scsb.DataSource = "10.208.15.29"; //from /system32/drivers/etc/hosts
            ////scsb.NetworkLibrary = "DBMSLPCN";
            ////scsb.InitialCatalog = "master";
            scsb.IntegratedSecurity = false;
            scsb.UserID = "cfserver";
            scsb.Password = "b1willrule";
            scsb.ConnectTimeout = 60;

            try
            {
                connection = new SqlConnection(scsb.ConnectionString); //Fire the ConnString
                connection.Open();
            }
            catch (Exception econn)
            {
                throw econn;
            }
        }

        public void MakeConnection(string DS)
        {
            //MSDE
            //scsb.DataSource = @"(local)";
            ////scsb.NetworkLibrary = "DBMSLPCN";
            ////scsb.InitialCatalog = "master";
            //scsb.IntegratedSecurity = false;
            //scsb.UserID = "sa";
            //scsb.Password = "bev1";
            //scsb.ConnectTimeout = 60;

            //LocalDB
            //scsb.DataSource = @"(localdb)\MSSQLLocalDB";
            ////scsb.NetworkLibrary = "DBMSLPCN";
            ////scsb.InitialCatalog = "master";
            //scsb.IntegratedSecurity = true;
            //scsb.UserID = "sa";
            //scsb.Password = "bev1";
            //scsb.ConnectTimeout = 60;

            //On DB2008
            scsb.DataSource = DS;
            ////scsb.NetworkLibrary = "DBMSLPCN";
            ////scsb.InitialCatalog = "master";
            scsb.IntegratedSecurity = false;
            scsb.UserID = "cfserver";
            scsb.Password = "b1willrule";
            scsb.ConnectTimeout = 60;

            try
            {
                connection = new SqlConnection(scsb.ConnectionString); //Fire the ConnString
                connection.Open();
            }
            catch (Exception econn)
            {
                throw econn;
            }
        }

        public DataTable GetGenericTable(string QueryString)
        {
            DataTable table = new DataTable();
            table.Locale = System.Globalization.CultureInfo.InvariantCulture;

            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(QueryString, connection);
                dataAdapter.Fill(table);
                return table;
            }
            catch (Exception E)
            {
                throw E;
            }

        }//GetGenericTable

        public void ExecuteNonQuery(string SQLString)
        {
            try
            {
                SqlCommand command = new SqlCommand(SQLString, connection);

                command.ExecuteNonQuery();
            }
            catch (Exception e1)
            {
                throw e1;
            }
        }//ExecuteNonQuery

        public object ExecuteScalar(string SQLString)
        {
            try
            {
                SqlCommand command = new SqlCommand(SQLString, this.connection);

                object value = command.ExecuteScalar();

                if (value is DBNull)
                {
                    return default(decimal);
                }
                else
                {
                    return value;
                }
            }
            catch (Exception e3)
            {
                throw e3;
            }
        }//ExecuteScalar

    }//class
}//namespace

